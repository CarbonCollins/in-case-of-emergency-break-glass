# Step 2 - Setting up the router

[[_TOC_]]

## Previous Step

[Step 1 - The Physical Box](./1-pysical-box.md)

## Description

This step involves switching to a standard router which will provide access to the internet once the server rack is decomissioned. This step is one of the first to be done so that you can still access this guide during the rest of the steps.

## Locating the modem

The internet is provided by a direct fibre line which enters the house in the living room. This can be found attached to the wall and will have 2 cables coming out from the bottom. More specific information can be found in the document within the emergency box.

## Setup

1. Obtain the router, network cable, and power supply from the emergency box.

2. Connect one end of the network cable into the '' port on the router

3. Disconnect the existing network cable attached to the modem and note what port it was connected to.

4. Connect the other end of the new network cable into the bottom of the modem in the same port as the one you just removed.

5. Connect the new router to power using the provided power supply.

6. Wait for the new WiFi to appear. The specific name and password will be contained within the document.

## Notifying the ISP

If I have perished or am very much incapacitated then you will need to notify the ISP that it has happened so that they can switch the internet service over to you. It is up to you how you proceed from here as you can probably decided to stop the contract fully, take over the contract to continue as is, ect.

With the previous setup steps done you will be able to continue with the current provider, switch to a new provider, or stop fully without too much issue as the router will already be ready to go for you.

Specifics as to which ISP, where to contact ect will be contained within the emergency document.

## Next Step

[Step 3 - Turning off the server rack](./3-turning-off-the-server-rack.md)
