# The Emergency Document

This is a template to be filled in with required information needed to complete all of the steps. The template itself will leave spaces blank to be filled in once printed out.

---

## Location of the modem

The modem where the fibre line enters the house can be found:
<pre>



</pre>

---

## WiFi

The pre-configured WiFi name and password needed to connect to the replacement router

### SSID
<pre></pre>

### Password
<pre></pre>

The admin username and password for the router is needed to make any changes to the router.

### Username
<pre></pre>

### Password
<pre></pre>

---

## Internet Service Provider (ISP)

The company that currently provides the internet to the house.

### Provider
<pre></pre>

### Contact
<pre>


</pre>

---

## Removal of the rack and servers

Removal and handling of the servers once they are shutdown is up to you, however, I would recommend contacting the person below as they may be able to help / be willing to adopt them.

### Contact
<pre></pre>

---
