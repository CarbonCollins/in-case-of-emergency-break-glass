# In case of emergency - Break Glass

[[_TOC_]]

## Description

This is a guide for those who need to decommission things after I am unable to do so anymore.

## Steps to take

#### [Step 1 - The physical box](./docs/1-pysical-box.md)
#### [Step 2 - Setting up the router](./docs/2-setting-up-the-router.md)
#### [Step 3 - Turning off the server rack](./docs/3-turning-off-the-server-rack.md)

## Document incomplete

This is the beginning of this document and will grow as things are setup. Some information is also not final and has not been added yet.
