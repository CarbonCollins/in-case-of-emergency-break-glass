# Step 1 - The Physical Box

[[_TOC_]]

## Location

The first thing to do is to find the physical `In case of emergency - Break Glass` box which is located under the staircase by the server rack.

## Contents

Within the box there will be several items which should be labeled

1. An emergency document that will contain more specific information regarding various companies to contact and more private information that will not be publicly available here.

2. A router pre-configured to get you back online.

3. A power supply and network cable for the router.

4. A key needed for accessing things.

## The Emergency Document Template

For item 1 in the boxes contents there is a template which can be printed out and filled in which can be found here at [The Emergency Document](./1a-emergency-document.md)

## Next Step

[Step 2 - Setting up the router](./2-setting-up-the-router.md)
