# Step 3 - Turning off the server rack

[[_TOC_]]


## Previous Step

[Step 2 - Setting up the router](./2-setting-up-the-router.md)

## Description

Once you have switched over to the emergency router there should not be anything stopping you from turning off the server rack as it is unlikely to be used anymore.

## The proper way

will add steps here soon...

## The quick way

Behind the rack will be 1-2 power plugs connected to the wall. Just disconnect them.

The rack will start beeping as the UPS will kick in. you now have 2 options.

1. Open the front door to the rack and the bottom most machine (the beeping one) should have an on/off/power button. press and hold it to shut down.
2. Wait an hour for the batteries to run out, you can push the power button once on the bottom machine to silence the beeping

## What to do with the servers...

This is kinda up to you at this point, they are yours now! There will however be a recommendation in the emergency document as to someone who may be able to help / be open to adopting some servers.

<!-- ## Next Step

[Step 4 - ](./4-) -->
